optee-client (4.0.0-1+apertis1) apertis; urgency=medium

  * Sync from debian/trixie.

 -- Dylan Aïssi <dylan.aissi@collabora.com>  Tue, 13 Feb 2024 10:10:42 +0100

optee-client (4.0.0-1) unstable; urgency=low

  * New upstream release.
    - FTCBFS fixed by the upstream (Closes: #1023233)

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Mon, 11 Dec 2023 01:07:42 +0800

optee-client (3.21.0-1) unstable; urgency=low

  * New upstream release.

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Mon, 26 Jun 2023 04:11:50 +0800

optee-client (3.20.0-1) unstable; urgency=low

  * New upstream release.

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Fri, 23 Jun 2023 02:01:10 +0800

optee-client (3.19.0-1+apertis1) apertis; urgency=medium

  * Add debian/apertis/copyright

 -- Dylan Aïssi <dylan.aissi@collabora.com>  Thu, 08 Feb 2024 15:40:15 +0100

optee-client (3.19.0-1+apertis0) apertis; urgency=medium

  * Import from Debian bookworm.
  * Set component to target.
  * Request from Bosch.

 -- Apertis package maintainers <packagers@lists.apertis.org>  Thu, 08 Feb 2024 14:35:02 +0000

optee-client (3.19.0-1) unstable; urgency=low

  * New upstream release.

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Tue, 18 Oct 2022 20:08:19 +0800

optee-client (3.18.0-1) unstable; urgency=low

  * New upstream release.

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Sat, 23 Jul 2022 16:23:00 +0200

optee-client (3.17.0-1) unstable; urgency=low

  * New upstream release.

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Tue, 19 Apr 2022 15:27:27 +0800

optee-client (3.16.0-2) unstable; urgency=low

  * Nothing changed. Just re-upload.
    For previous upload, we add a new package so we have to send binary
    upload to the new queue. This time we upload source-only package.

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Mon, 07 Feb 2022 10:18:56 +0800

optee-client (3.16.0-1) unstable; urgency=low

  * New upstream release.
    - Add libseteec0 package.

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Fri, 04 Feb 2022 23:49:24 +0800

optee-client (3.15.0-1) unstable; urgency=low

  * New upstream release.

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Sun, 14 Nov 2021 17:50:01 +0800

optee-client (3.13.0-1) unstable; urgency=low

  * Initial release (Closes: #868215)

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Thu, 20 May 2021 09:18:38 +0800
